require('kohanajs').addNodeModule(__dirname);

module.exports = {
  Auth: require('./classes/Auth'),
  ControllerAuth: require('./classes/controller/Auth'),
  ControllerAccount: require('./classes/controller/Account'),
  ControllerHome: require('./classes/controller/Home'),
  ControllerMixinAuth: require('./classes/controller-mixin/Auth'),
  ControllerMixinEmailRegister: require('./classes/controller-mixin/EmailRegister'),
  ControllerMixinEmailAccount: require('./classes/controller-mixin/EmailAccount'),
  ControllerMixinLoginRequire: require('./classes/controller-mixin/LoginRequire'),
  HelperRegistrar: require('./classes/helper/Registrar'),
  ModelPasswordIdentifier: require('./classes/model/PasswordIdentifier'),
  ModelPerson: require('./classes/model/Person'),
  ModelRole: require('./classes/model/Role'),
  ModelUser: require('./classes/model/User'),
};
