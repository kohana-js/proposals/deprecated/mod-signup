const { KohanaJS } = require('kohanajs');

module.exports = {
  databasePath: `${KohanaJS.EXE_PATH}/../database`,
  salt: 'thisislonglonglonglongtextover32bytes',
};
