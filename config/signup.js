const { MailAdapter } = require('@kohanajs/mod-mail');

module.exports = {
  destination: '/account',
  allowPostAssignRoleID: false,
  requireActivate: true,
  defaultRoleID: 1,
  forgotPassword: {
    salt: 'anotherlongsaltAnotherLongLongSalt',
  },
  activate: {
    salt: 'anotherlongsaltAnotherLongLongSalt',
  },
  mailAdapter: MailAdapter,
  rootUser: 'root',
};
