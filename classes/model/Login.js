const {ORM} = require('kohanajs');

class Login extends ORM{
  user_id = null;
  ip = null;
  note = null;

  static joinTablePrefix = 'login';
  static tableName = 'logins';

  static fields = new Map([
    ["ip", "String"],
    ["note", "String"]
  ]);
  static belongsTo = new Map([
    ["user_id", "User"]
  ]);
}

module.exports = Login;
