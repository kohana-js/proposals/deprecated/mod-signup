const crypto = require('crypto');
const { KohanaJS } = require('kohanajs');
const { HelperMail, MailAdapter } = require('@kohanajs/mod-mail');

class HelperRegistrar {
  #helperMail

  #domain

  /**
   *
   * @param clientIP
   * @param domain
   * @param opts
   * @param opts.mailDB
   * @param opts.mailAdapter
   * @param opts.mailTemplatePath
   */
  constructor(domain, clientIP, opts = {}) {
    const {
      mailDB = undefined,
      mailAdapter = MailAdapter,
      mailTemplatePath = undefined,
    } = opts;

    this.#helperMail = new HelperMail(domain, clientIP, {
      database: mailDB,
      adapter: mailAdapter,
      templatePath: mailTemplatePath,
    });
    this.#domain = domain;
  }

  static getActivateCode(username, email, salt) {
    // generate activate code
    const activateCode = crypto.createHash('md5');
    activateCode.update(username + email + salt);
    return activateCode.digest('hex');
  }

  /**
   *
   * @param username
   * @param email
   * @param opts
   * @param opts.language
   * @returns {Promise<*>}
   */
  async sendActivateCode(username, email, opts = {}) {
    const language = opts.language || 'en';
    const config = KohanaJS.config.edm;

    const options = { salt: KohanaJS.config.signup.activate.salt,
      sender: config.mail.sender,
      subject: config.mail.activateCode.subject.get(language) || config.mail.activateCode.subject.get(''),
      text: config.mail.activateCode.text.get(language) || config.mail.activateCode.text.get(''),
      html: config.mail.activateCode.html.get(language) || config.mail.activateCode.html.get(''),
      landing: config.mail.activateCode.landing.get(language) || config.mail.activateCode.landing.get(''),
      ...opts };

    //    ({subject, text, html, landing}) => ({subject, text, html, landing})(config.mail.activateCode)
    const code = HelperRegistrar.getActivateCode(username, email, options.salt);
    const url = `https://${this.#domain}/${options.landing}/${code}`;

    return this.#helperMail.send(options.subject, options.text, options.sender, email, {
      htmlTemplate: options.html,
      tokens: {
        email,
        language,
        code,
        url,
      },
    });
  }

  /**
   *
   * @param username
   * @param resetCode
   * @param opts
   * @param opts.salt
   * @returns {string}
   */
  static resetPasswordSignature(username, resetCode, opts = {}) {
    const { salt = KohanaJS.config.signup.forgotPassword.salt } = opts;

    const sign = crypto.createHash('md5');
    sign.update(username + resetCode + salt);
    return sign.digest('hex');
  }

  /**
   *
   * @param username
   * @param email
   * @param resetCode
   * @param opts
   * @param opts.language
   * @param opts.salt
   * @param opts.sender
   * @param opts.subject
   * @param opts.text
   * @param opts.html
   * @param opts.landing
   * @returns {Promise<*>}
   */
  async sendResetPassword(username, email, resetCode, opts = {}) {
    const language = opts.language || 'en';
    const config = KohanaJS.config.edm;

    const {
      salt = KohanaJS.config.signup.forgotPassword.salt,
      sender = config.mail.sender,
      subject = config.mail.resetPassword.subject.get(language) || config.mail.resetPassword.subject.get(''),
      text = config.mail.resetPassword.text.get(language) || config.mail.resetPassword.text.get(''),
      html = config.mail.resetPassword.html.get(language) || config.mail.resetPassword.html.get(''),
      landing = config.mail.resetPassword.landing.get(language) || config.mail.resetPassword.landing.get(''),
      tokens = {},
    } = opts;

    const sign = HelperRegistrar.resetPasswordSignature(username, resetCode, salt);
    const url = `https://${this.#domain}/${landing}/${sign}/${resetCode}`;

    return this.#helperMail.send(subject, text, sender, email, {
      htmlTemplate: html,
      tokens: Object.assign(tokens, {
        email,
        language,
        url,
      }),
    });
  }

  /**
   *
   * @param username
   * @param email
   * @param opts
   * @param opts.language
   * @param opts.sender
   * @param opts.subject
   * @param opts.text
   * @param opts.html
   * @returns {Promise<*>}
   */
  async sendUsername(username, email, opts = {}) {
    const language = opts.language || 'en';
    const config = KohanaJS.config.edm;

    const {
      sender = config.mail.sender,
      subject = config.mail.username.subject.get(language) || config.mail.username.subject.get(''),
      text = config.mail.username.text.get(language) || config.mail.username.text.get(''),
      html = config.mail.username.html.get(language) || config.mail.username.html.get(''),
      tokens = {},
    } = opts;

    return this.#helperMail.send(subject, text, sender, email, {
      htmlTemplate: html,
      tokens: Object.assign(tokens, {
        email,
        language,
        username,
      }),
    });
  }
}

module.exports = HelperRegistrar;
