const { ControllerMixin } = require('@kohanajs/core-mvc');
const { KohanaJS, ControllerMixinDatabase } = require('kohanajs');
const { ControllerMixinMultipartForm } = require('@kohanajs/mod-form');
const Auth = KohanaJS.require('Auth');

class ControllerMixinAuth extends ControllerMixin {
  static USER = 'user';

  static async action_auth(state) {
    const client = state.get(ControllerMixin.CLIENT);
    const { request } = client;
    const { session } = request;
    const database = state.get(ControllerMixinDatabase.DATABASES).get('admin');

    const $_POST = state.get(ControllerMixinMultipartForm.POST_DATA);
    const username = $_POST.username.trim();
    const password = $_POST.password.trim();

    try {
      const auth = await Auth.authorize(username, password, database, KohanaJS.config.auth.salt, { ip: client.clientIP });
      state.set(this.USER, auth.user);
      Object.assign(session, auth.session);
    } catch (e) {
      client.error = e;
    }
  }

  static async action_logout(state) {
    const client = state.get(ControllerMixin.CLIENT);
    const { request } = client;
    Auth.logout(request.session);
  }
}

module.exports = ControllerMixinAuth;
