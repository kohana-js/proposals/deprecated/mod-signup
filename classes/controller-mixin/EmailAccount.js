const { ControllerMixin } = require('@kohanajs/core-mvc');
const { KohanaJS, ORM, ControllerMixinDatabase } = require('kohanajs');
const { ControllerMixinMultipartForm } = require('@kohanajs/mod-form');
const HelperRegistrar = require('../helper/Registrar');
const Auth = require('../Auth');

const PasswordIdentifier = ORM.require('PasswordIdentifier');
const User = ORM.require('User');
const Person = ORM.require('Person');

class ControllerMixinEmailAccount extends ControllerMixin {
  static INSTANCE = 'user';

  static MAIL_ADAPTER = 'mail_adapter';

  static #HELPER_REGISTRAR = 'helper_registrar';

  static async before(state) {
    const client = state.get('client');
    const { request } = client;
    const { session } = request;
    const { hostname } = request;
    const { clientIP } = client;
    const adminDB = state.get(ControllerMixinDatabase.DATABASES).get('admin');
    const mailDB = state.get(ControllerMixinDatabase.DATABASES).get('mail');

    state.set(this.INSTANCE, await ORM.factory(User, session.user_id, { database: adminDB, limit: 1 }));
    state.set(this.#HELPER_REGISTRAR, state.get(this.#HELPER_REGISTRAR) || new HelperRegistrar(hostname, clientIP, { mailDB, mailAdapter: state.get(this.MAIL_ADAPTER) }));
  }

  static async action_activate(state) {
    const client = state.get(ControllerMixin.CLIENT);
    const { request } = client;

    const { code } = request.params;
    const userId = request.session.user_id;
    const instance = state.get(this.INSTANCE);
    const person = await instance.parent('person_id');
    const adminDB = state.get(ControllerMixinDatabase.DATABASES).get('admin');

    if (state.get(this.#HELPER_REGISTRAR).activateCode(instance.name, person.email, KohanaJS.config.signup.activate.salt) !== code) {
      throw new Error('invalid activate code');
    }

    const identifier = await ORM.readBy(PasswordIdentifier, 'user_id', [userId], { database: adminDB, limit: 1 });
    identifier.verified = true;
    await identifier.write();
  }

  static async action_resend_verification(state) {
    const client = state.get(ControllerMixin.CLIENT);
    const { session } = client.request;
    const helperRegister = state.get(this.#HELPER_REGISTRAR);
    const adminDB = state.get(ControllerMixinDatabase.DATABASES).get('admin');

    const userId = session.user_id;
    const user = await ORM.factory(User, userId, { database: adminDB, limit: 1 });
    const person = await user.parent('person_id');

    await helperRegister.sendActivateCode(user.name, person.email);
  }

  static async action_change_email_post(state) {
    const client = state.get('client');
    const { session } = client.request;
    const $_POST = state.get('$_POST');

    const { email } = $_POST;
    const { password } = $_POST;
    const adminDB = state.get(ControllerMixinDatabase.DATABASES).get('admin');

    // verify password
    const userId = session.user_id;
    const identifier = await ORM.readBy(PasswordIdentifier, 'user_id', [userId], { database: adminDB, limit: 1 });
    const user = await identifier.parent('user_id');

    const hash = Auth.hashPassword(user.name, password, KohanaJS.config.auth.salt);

    if (identifier.password !== hash) {
      console.log(identifier.password, hash);
      throw new Error('Password mismatch');
    }

    // check email exist
    const res = await ORM.readBy(PasswordIdentifier, 'identifier', [`email:${email}`], { database: adminDB, limit: 1 });

    if (res) {
      throw new Error(`Email ${email} already in use. Please use another email`);
    }

    // password is match, generate new password hash
    const person = await ORM.factory(Person, user.person_id, { database: adminDB, limit: 1 });
    person.email = email;
    await person.write();

    identifier.identifier = `email:${email}`;
    identifier.verified = !KohanaJS.config.signup.requireActivate;
    await identifier.write();

    if (KohanaJS.config.signup.requireActivate) {
      const helperRegister = state.get(this.#HELPER_REGISTRAR);
      await helperRegister.sendActivateCode(user.name, email);
    }
  }

  static async action_change_password_post(state) {
    const client = state.get(ControllerMixin.CLIENT);
    const {
      'old-password': oldPassword,
      'new-password': newPassword,
      'retype-password': retypePassword,
    } = state.get(ControllerMixinMultipartForm.POST_DATA);
    const adminDB = state.get(ControllerMixinDatabase.DATABASES).get('admin');

    //    const oldPassword = $_POST['old-password'];
    //    const newPassword = $_POST['new-password'];
    //    const retypePassword = $_POST['retype-password'];

    if (newPassword !== retypePassword) throw new Error('new password and retype password mismatch');
    const { user_id: userId } = client.request.session;

    const user = await ORM.factory(User, userId, { database: adminDB, limit: 1 });
    const identifier = await ORM.readBy(PasswordIdentifier, 'user_id', [user.id], { database: adminDB, limit: 1 });
    const hash = Auth.hashPassword(user.name, oldPassword, KohanaJS.config.auth.salt);
    if (identifier.password !== hash) throw new Error('old password mismatch');

    identifier.password = Auth.hashPassword(user.name, newPassword, KohanaJS.config.auth.salt);
    await identifier.write();
  }
}

module.exports = ControllerMixinEmailAccount;
